json.extract! temporada, :id, :numero_temporada, :created_at, :updated_at
json.url temporada_url(temporada, format: :json)