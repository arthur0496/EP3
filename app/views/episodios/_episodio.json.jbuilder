json.extract! episodio, :id, :titulo, :sinopse, :assistido, :numero_episodio, :created_at, :updated_at
json.url episodio_url(episodio, format: :json)