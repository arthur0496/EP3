require 'test_helper'

class EpisodiosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @episodio = episodios(:one)
  end

  test "should get index" do
    get episodios_url
    assert_response :success
  end

  test "should get new" do
    get new_episodio_url
    assert_response :success
  end

  test "should create episodio" do
    assert_difference('Episodio.count') do
      post episodios_url, params: { episodio: { assistido: @episodio.assistido, numero_episodio: @episodio.numero_episodio, sinopse: @episodio.sinopse, titulo: @episodio.titulo } }
    end

    assert_redirected_to episodio_url(Episodio.last)
  end

  test "should show episodio" do
    get episodio_url(@episodio)
    assert_response :success
  end

  test "should get edit" do
    get edit_episodio_url(@episodio)
    assert_response :success
  end

  test "should update episodio" do
    patch episodio_url(@episodio), params: { episodio: { assistido: @episodio.assistido, numero_episodio: @episodio.numero_episodio, sinopse: @episodio.sinopse, titulo: @episodio.titulo } }
    assert_redirected_to episodio_url(@episodio)
  end

  test "should destroy episodio" do
    assert_difference('Episodio.count', -1) do
      delete episodio_url(@episodio)
    end

    assert_redirected_to episodios_url
  end
end
