class CreateSeries < ActiveRecord::Migration[5.0]
  def change
    create_table :series do |t|
      t.string :titulo
      t.text :sinopse

      t.timestamps
    end
  end
end
