class AddTemporadaToEpisodio < ActiveRecord::Migration[5.0]
  def change
    add_reference :episodios, :temporada, foreign_key: true
  end
end
