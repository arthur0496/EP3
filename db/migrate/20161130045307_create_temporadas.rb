class CreateTemporadas < ActiveRecord::Migration[5.0]
  def change
    create_table :temporadas do |t|
      t.integer :numero_temporada

      t.timestamps
    end
  end
end
