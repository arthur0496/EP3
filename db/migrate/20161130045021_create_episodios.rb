class CreateEpisodios < ActiveRecord::Migration[5.0]
  def change
    create_table :episodios do |t|
      t.string :titulo
      t.text :sinopse
      t.boolean :assistido
      t.integer :numero_episodio

      t.timestamps
    end
  end
end
